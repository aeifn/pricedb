import json
import requests
from datetime import date
import os

apikey = os.getenv('APIKEY')

start_date="2022-12-28"
end_date=date.today().strftime("%Y-%m-%d")
currencies = [
        "EUR",
        "RUB",
        "KZT",
        "TRY",
        "EGP",
        "JOD",
        "AED",
        "OMR",
        "AMD",
        "RSD"
        ]

url = "https://api.apilayer.com/exchangerates_data/timeseries?start_date=" + start_date + "&end_date=" + end_date + "&base=USD&symbols=" + ",".join(currencies)

payload = {}
headers= {
  "apikey": apikey
}

response = requests.request("GET", url, headers=headers, data = payload)

status_code = response.status_code
data = response.json()
for date in data["rates"]:
    for currency in data["rates"][date]:
        arr = ["P", date, "USD", str(data["rates"][date][currency]), currency]
        print(" ".join(arr))
